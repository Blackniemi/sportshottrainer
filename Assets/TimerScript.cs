﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    UIScript uiScript;

    AudioSource audioPlayer;
    AudioClip timerBeepClip;
    AudioClip trainingCompleteClip;
    AudioClip trainingBeginsClip;
    AudioClip readyClip;

    Text commandText;
    Text goButtonText;
    Text timeLeftText;

    ToggleGroup waitTimeGroup;
    ToggleGroup variationGroup;
    ToggleGroup durationGroup;

    public int waitTime;
    public int timeVariation;
    public int duration;
    public float timerTimeS;
    public float timeLeft;

    public bool timerRunning;
    public bool singleRun;

	// Use this for initialization
	void Start () {

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        uiScript = GameObject.Find("MASTER_OBJ").GetComponent<UIScript>();

        audioPlayer = GameObject.Find("MASTER_OBJ").GetComponent<AudioSource>();
        timerBeepClip = Resources.Load<AudioClip>("Audio/timer_beep");
        trainingCompleteClip = Resources.Load<AudioClip>("Audio/training_complete");
        trainingBeginsClip = Resources.Load<AudioClip>("Audio/training_begins");
        readyClip = Resources.Load<AudioClip>("Audio/ready");

        commandText = GameObject.Find("CommandText").GetComponent<Text>();
        goButtonText = GameObject.Find("GoButtonText").GetComponent<Text>();
        timeLeftText = GameObject.Find("TimeLeftText").GetComponent<Text>();
        waitTimeGroup = GameObject.Find("WaitTimeToggleGroup").GetComponent<ToggleGroup>();
        variationGroup = GameObject.Find("VariationToggleGroup").GetComponent<ToggleGroup>();
        durationGroup = GameObject.Find("DurationToggleGroup").GetComponent<ToggleGroup>();

        commandText.text = "";
        goButtonText.text = "GO";
        timeLeftText.text = "--:--";
        singleRun = true;

        UpdateWaitTime();
        UpdateVariation();
        UpdateDuration();
    }
	
	// Update is called once per frame
	void Update () {
		

        if (timerRunning)
        {
            timeLeft -= Time.deltaTime;

            if(!singleRun)
            {
                int minutes = Mathf.FloorToInt(timeLeft / 60);
                int seconds = Mathf.FloorToInt(timeLeft - minutes*60f);
                timeLeftText.text = minutes.ToString() + ":" + seconds.ToString();
            }

            if (timerTimeS <= 0f  || timeLeft <= 0f)
            {
                TimerZero();
            }
            else
            {
                timerTimeS -= Time.deltaTime;
            }
        }


	}

    public void UpdateWaitTime()
    {
        foreach (Toggle t in waitTimeGroup.ActiveToggles())
        {
            if (int.TryParse(t.gameObject.transform.Find("Label").gameObject.GetComponent<Text>().text, out waitTime))
                break;
            Debug.Log("Serious error");
        }
    }

    public void UpdateVariation()
    {
        foreach (Toggle t in variationGroup.ActiveToggles())
        {
            int.TryParse(t.gameObject.transform.Find("Label").gameObject.GetComponent<Text>().text, out timeVariation);
            break;
        }
    }

    public void UpdateDuration()
    {
        foreach (Toggle t in durationGroup.ActiveToggles())
        {
            string text = t.gameObject.transform.Find("Label").gameObject.GetComponent<Text>().text;

            if (text == "single")
            {
                singleRun = true;
                timeLeft = 999f;
                break;
            }
            else
            {
                singleRun = false;
            }
                
            int.TryParse(text, out duration);
            timeLeft = duration * 60;
            break;
        }
    }

    public void TimerZero()
    {
        timerRunning = false;
        audioPlayer.PlayOneShot(timerBeepClip);

        if (timeLeft > 4f && !singleRun)
        {
            StartTimer();
        }
        else
        {
            EndTraining();
        }

    }

    public void EndTraining()
    {
        timerRunning = false;
        timeLeftText.text = "--:--";
        goButtonText.text = "GO";
        commandText.text = "";
        uiScript.toggleGoButtonPressed(false);
        StartCoroutine(PlayEnd());
    }

    public void Begintraining()
    {
        goButtonText.text = "STOP";
        StopAllCoroutines();
        StartCoroutine(PlayBegin());
    }

    private IEnumerator PlayEnd()
    {
        yield return new WaitForSeconds(2f);
        Debug.Log("playing clip");
        audioPlayer.clip = trainingCompleteClip;
        audioPlayer.Play();
        Debug.Log("playing clip2");
    }

    private IEnumerator PlayBegin()
    {
        commandText.text = "READY";
        audioPlayer.clip = trainingBeginsClip;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length + 1f);
        commandText.text = "TRAIN!";
        audioPlayer.clip = readyClip;
        audioPlayer.Play();
        yield return new WaitForSeconds(audioPlayer.clip.length);
        UpdateDuration();
        StartTimer();
    }

    public void StartTimer()
    {
        float t = (float)waitTime;
        float v = (float)timeVariation;

        if (timeVariation != 0)
        {
            timerTimeS = Random.Range(t - v, t + v);
        }
        else
        {
            timerTimeS = t;
        }
        
        timerRunning = true;
    }

}
