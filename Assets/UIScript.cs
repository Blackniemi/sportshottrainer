﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

    ScrollRect scrollRect;
    Image goButtonImage;

    GameObject mainMenuUI;
    GameObject basicModeUI;
    GameObject splitsModeUI;

    GameObject basicModeSettingsUI;

    ToggleGroup ThemeColorToggleGroup;

    Button backButton;

    // Use this for initialization
    void Start () {
        scrollRect = GameObject.Find("AppScrollView").GetComponent<ScrollRect>();
        goButtonImage = GameObject.Find("GoButton").GetComponent<Button>().GetComponent<Image>();

        mainMenuUI = GameObject.Find("MainContent");
        basicModeUI = GameObject.Find("BasicModeContent");

        basicModeSettingsUI = GameObject.Find("BasicModeSettings");

        ThemeColorToggleGroup = GameObject.Find("ThemeColorToggleGroup").GetComponent<ToggleGroup>();

        backButton = GameObject.Find("BackButton").GetComponent<Button>();

        basicModeUI.SetActive(false);

        LoadPrefs();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SelectBasicMode()
    {
        basicModeUI.SetActive(true);
        mainMenuUI.SetActive(false);
    }

    public void SelectSplitsMode()
    {

    }

    public void BackMenuButton()
    {
        mainMenuUI.SetActive(true);
        basicModeUI.SetActive(false);
        splitsModeUI.SetActive(false);
    }

    public void toggleGoButtonPressed(bool pressed)
    {
        if (pressed)
        {
            basicModeSettingsUI.SetActive(false);
            backButton.interactable = false;
        }
        else
        {
            basicModeSettingsUI.SetActive(true);
            backButton.interactable = true;
        }
    }

    public void SetThemeColor()
    {
        Color themeColor = Color.white;

        foreach (Toggle t in ThemeColorToggleGroup.ActiveToggles())
        {
            themeColor = t.gameObject.transform.Find("Background").gameObject.GetComponent<Image>().color;
            PlayerPrefs.SetString("prefs_theme_color", t.gameObject.name);
            break;
        }

        mainMenuUI.GetComponent<Image>().color = themeColor;
        basicModeUI.GetComponent<Image>().color = themeColor;
        splitsModeUI.GetComponent<Image>().color = themeColor;
    }

    void LoadPrefs()
    {
        string prefThemeCol = PlayerPrefs.GetString("prefs_theme_color");
        ThemeColorToggleGroup.SetAllTogglesOff();

        GameObject.Find(prefThemeCol).GetComponent<Toggle>().isOn = true;

        SetThemeColor();
    }
    public void ToggleSettings()
    {
        if (scrollRect.normalizedPosition.y > 0.5f)
        {
            scrollRect.normalizedPosition = new Vector2(0, 0);
        }
        else
        {
            scrollRect.normalizedPosition = new Vector2(0, 1);
        }
    }

}
