﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputScript : MonoBehaviour {


    TimerScript timerScript;
    UIScript uiScript;

	// Use this for initialization
	void Start () {
        timerScript = GameObject.Find("MASTER_OBJ").GetComponent<TimerScript>();
        uiScript = GameObject.Find("MASTER_OBJ").GetComponent<UIScript>();
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
            }
            else
            {
                Application.Quit();
            }
        }


    }

    public void GoButtonPressed()
    {

        if (timerScript.timerRunning)
        {
            timerScript.EndTraining();
            uiScript.toggleGoButtonPressed(false);
        }
        else
        {
            timerScript.Begintraining();
            uiScript.toggleGoButtonPressed(true);
        }
    }

}
